#!/bin/bash
read n
a=0
b=1
for arg in $( seq $n )
do
	a=$(echo $a+$b | bc | sed -e 's/[^0-9]//' | tr -d '\n')
	b=$(echo $a-$b | bc | sed -e 's/[^0-9]//' | tr -d '\n')	
done
echo $a
