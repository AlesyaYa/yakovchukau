#!/bin/bash
dir_depth(){
    local dir
    cd -- "$1"
    ((counter++))

    for dir in *
    do
      if [ -d "$dir" ]
      then
        dir_depth "$dir"
      fi
    done
    if ((counter > max))
    then
        max=$counter
    fi
    ((counter--))
    if (( counter == 0 ))
    then
        echo $max
        unset counter
    else
        cd ..
    fi
}
n=$(dir_depth "$@")
m=$(expr $n - 1)
for arg in $( seq $m )
do
	basename -a $(find -maxdepth $arg -mindepth $arg | sed -e 's/$/ /' | tr -d '\n') | sort
	echo 
done

str=$(find -maxdepth $n -mindepth $n | sed -e 's/$/ /' | tr -d '\n')
len=$(echo ${#str})
if (( $len>0 )); then
	basename -a $str | sort;
fi
